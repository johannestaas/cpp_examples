#include <sys/uio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>

struct Data {
    unsigned flag:1;
    unsigned four:4;
    unsigned three:3;
};

int main(int argc, char *argv[]) {
    int fd;
    struct iovec vec;
    struct Data d1;
//    d1.flag = 0x1;
//    d1.four = 4;
//    d1.three = 2;
    if((fd = open("testing_vec", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR)) == -1) {
        perror("open");
        abort();
    }
    vec.iov_base = &d1;
    vec.iov_len = 1;
//    writev(fd, &vec, 1);

    if(readv(fd, &vec, 1) == -1) {
        perror("read");
        abort();
    }
    printf("flag: %d\nfour: %d\nthree: %d\n", d1.flag, d1.four, d1.three);
    if(close(fd) == -1) {
        perror("close");
        abort();
    }
    return 0;
}
    
