#define EXIT_BAD_OPTS 1

#include <string>
#include <boost/program_options.hpp>

using namespace std;

namespace prog_opts = boost::program_options;

int main(int argc, char* argv[]) {
  bool b_version_flag;
  string script_path;
  
  prog_opts::options_description prog_desc("usage: compiler <script>");
  prog_opts::positional_options_description prog_pos;
  prog_pos.add("script", -1);

  prog_desc.add_options()
    ("help,h", "produce help message")
    ("version,V", prog_opts::value(&b_version_flag)->zero_tokens())
    ("script", prog_opts::value< vector<string> >(), "script")
  ;
  prog_opts::variables_map args;
  
  try {
    prog_opts::store(
      prog_opts::command_line_parser(argc, argv)
      .options(prog_desc)
      .positional(prog_pos)
      .run(), args);
  } catch (boost::program_options::unknown_option) {
    cout << prog_desc;
    return EXIT_BAD_OPTS;
  }

  prog_opts::notify(args);

  if (args.count("help")) {
    cout << prog_desc;
    return 0;
  }

  if (b_version_flag) {
    cout << "progopts v0.9" << endl;
    return 0;
  }

  if (args.count("script")) {
    cout << "Pos arg: " << args["script"].as< vector<string> >().front() 
      << endl;
  }

  return 0;
}

