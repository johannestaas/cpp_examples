#include <iostream>
#include <vector>
#include <memory>
using namespace std;

class Base {
public:
    Base() {};
    ~Base() {};
    virtual void talk()=0;
};

class Cat: public Base {
public:
    Cat() {};
    ~Cat() {};
    void talk() {
        cout << "Mew!\n";
    }
};

class Dog: public Base {
public:
    Dog() {}; 
    ~Dog() {};
    void talk() {
        cout << "woof!\n";
    }
};

int main() {
    Cat *c = new Cat();
    Dog *d = new Dog();
    c->talk();
    d->talk();

    Base *bases[2] = {c, d};
    for(int i=0; i<2; i++) {
        bases[i]->talk();
    }
    delete c;
    delete d;
}

