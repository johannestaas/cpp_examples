#include "shape.h"

Shape::Shape() {
    this->x = 0;
    this->y = 0;
}

void Shape::set_pos(int x, int y) {
    this->x = x;
    this->y = y;
}

int Shape::get_x() {
    return this->x;
}

int Shape::get_y() {
    return this->y;
}

