#include <iostream>
#include "shape.h"

using namespace std;

int main(int argc, char *argv[]) {
    Shape *s;
    cout << "Instanciating shape." << endl;
    s = new Shape();
    cout << "Shapes coord: " << s->get_x() << ", " << s->get_y() << endl;
    cout << "Setting shape pos to 1, 22." << endl;
    s->set_pos(1, 22);
    cout << "Shapes coord: " << s->get_x() << ", " << s->get_y() << endl;
    return 0;
}
