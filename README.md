cpp_examples
============

Just a few examples to reference when using basic features of C++

*   cli_args

    Parse command-line arguments.

*   file_io

    How to output to a file with fstream.

*   log4cpp

    How to do basic logging with the log4cpp library.

*   ncurses

    Hello World with ncurses. Will add more later, with input.

*   sdl (stripped down arkasm)

    Displays graphics with libsdl (depends on it obviously). This is also
    a good example of how to do logging with log4cpp, and how to incorporate
    Makefiles. Depends on boost too, but I may have stripped out the
    networking already. 

*   simple_classes

    Just some very basic classes to show the syntax.
