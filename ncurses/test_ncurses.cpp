#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <curses.h>

int main() {
    WINDOW *win;
    if ((win = initscr()) == NULL) {
        fprintf(stderr, "Error init ncurses.\n");
        exit(1);
    }
    mvaddstr(13, 33, "Hello, world!");
    refresh();
    sleep(3);
    
    delwin(win);
    endwin();
    refresh();

    return 0;
}