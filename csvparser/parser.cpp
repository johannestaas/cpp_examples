#include <string>
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>

namespace csv_parser {

    void tokenize_line(
        std::vector<std::string>& str_v, 
        std::string line, 
        const std::string delim) {
        size_t pos = 0;
        std::string token;
        while ((pos = line.find(delim)) != std::string::npos) {
            std::string token = line.substr(0, pos);
            str_v.push_back(token);
            line.erase(0, pos + delim.length());
        }
        str_v.push_back(line);
    }

    bool check_column(const std::string& str) {
        size_t n = std::count(str.begin(), str.end(), '"');
        if (n == 0) {
            return true;
        } else if (n != 2) {
            return false;
        } else {
            return str[0] == '"' && str[str.length()-1] == '"';
        }
    }
        
    void check_filepath(
        const std::string path,
        const std::string delim) {
        std::cout << "Checking file " << path << std::endl;
        std::ifstream infile(path.c_str());
        std::string line;
        unsigned long i = 0;
        while (std::getline(infile, line)) {
            std::vector<std::string> str_v;
            i++;
            tokenize_line(str_v, line, delim);
            for (const std::string& col: str_v) {
                if (!check_column(col)) {
                    std::cout << "Line [" << i << "]: " << col << std::endl;
                }
            }
        }
    }
}

int main(int argc, char* argv[]) {
    for (int i=1; i<argc; i++) {
        csv_parser::check_filepath(argv[i], "|");
    }
}
