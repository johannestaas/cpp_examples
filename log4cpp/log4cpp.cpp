#include <iostream>
#include <stdlib.h>
#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>

int main(int argc, char** argv) {
const char *file_log4cpp_init = "logging.properties";

    try {
        log4cpp::PropertyConfigurator::configure( file_log4cpp_init );
    }
    catch(log4cpp::ConfigureFailure &e) {
        std::cout 
            << e.what() 
            << " [log4cpp::ConfigureFailure catched] while reading " 
            << file_log4cpp_init 
            << std::endl;
        exit(1);
    }
    log4cpp::Category &log_root = log4cpp::Category::getRoot();
    log_root.warn("warning!");
    return 0;
}
