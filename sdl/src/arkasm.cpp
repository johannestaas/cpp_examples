#include <iostream>
#include "log.h"
#include "window.h"

int main(int argc, char* argv[]) {
	Log_d("main()");

    Log_d("Opening window...");
    window::setup();
    window::title();
    Log_d("Opened.");
    
    while (true) {
        window::draw();
        char msg[80];
        std::cin.getline(msg, 79);
        if (strncmp(msg, "q", 1) == 0)
            break;
    }

    Log_d("Shutting down...");
    window::shutdown();
    Log_d("Gracefully exiting.");
    return 0;
}

