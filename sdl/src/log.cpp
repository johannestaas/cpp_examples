#include <assert.h>
#include <fstream>
#include <string>

#include "log.h"

Log* Log::m_instance = NULL;

const std::string currentDateTime() {
    time_t now = time(0);
    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%X", &tstruct);
    return buf;
}

void setStream(std::ostream** stream, const std::string line)
{
	if (!line.compare("0")) {
		*stream = 0;
	} else if (!line.compare("1")) {
		*stream = &std::cout;
	} else {
		*stream = new std::ofstream(line.c_str(), std::ios::app);
		**stream << std::endl << "#Logger started at " << currentDateTime() << std::endl;
	}
}

void Log::init()
{
	std::string line;
	std::ifstream configFile;
	configFile.open("log.conf");
	assert(configFile.is_open());

	getline(configFile, line);
	setStream(&dstream, line);

	getline(configFile, line);
	setStream(&estream, line);

	getline(configFile, line);
	setStream(&istream, line);

	getline(configFile, line);
	setStream(&vstream, line);

	getline(configFile, line);
	setStream(&wstream, line);
}

Log* Log::getSingleton()
{
	if (!m_instance) {
		m_instance = new Log;
		m_instance->init();
	}
	return m_instance;
}

void Log::log(const char level, const char* file, const int line, const char* message)
{
	std::ostream* out;
	switch (level) {
	case 'D':
		out = getSingleton()->dstream;
		break;
	case 'E':
		out = getSingleton()->estream;
		break;
	case 'I':
		out = getSingleton()->istream;
		break;
	case 'V':
		out = getSingleton()->vstream;
		break;
	case 'W':
		out = getSingleton()->wstream;
		break;
	}

	if (out) {
		*out << level << ", " << currentDateTime() << ", " << file << ":" << line << ", " << message << std::endl;
	}
}
