#ifndef HEADERFILE_H
#define HEADERFILE_H

#include <iostream>

#define Log_d(message) Log::log('D', __FILE__, __LINE__, message)
#define Log_e(message) Log::log('E', __FILE__, __LINE__, message)
#define Log_i(message) Log::log('I', __FILE__, __LINE__, message)
#define Log_v(message) Log::log('V', __FILE__, __LINE__, message)
#define Log_w(message) Log::log('W', __FILE__, __LINE__, message)

class Log {

public:
	static void log(const char level, const char* file, const int line, const char* message);

private:
	static Log* m_instance;
	static Log* getSingleton();
	
    void init();
	std::ostream* dstream;
	std::ostream* estream;
	std::ostream* istream;
	std::ostream* vstream;
	std::ostream* wstream;
    
    Log() {};                      // Private constructor
	~Log() {};                     // Private destructor
	Log(Log const&) {};            // Private copy constructor
	Log& operator = (Log const&);  // Private assignment operator
};

#endif
