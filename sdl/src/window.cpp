#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include "window.h"

namespace window {

    SDL_Surface *WINDOW = NULL;
    Scene *SCENE = NULL;

    const int WIDTH = 800;
    const int HEIGHT = 600;
    const char *TITLE = "Arkasm";
    TTF_Font* FONT = TTF_OpenFont("res/fonts/FreeSans.ttf", 12);
    TTF_Font* FONT_BIG = TTF_OpenFont("res/fonts/FreeSans.ttf", 24);
    SDL_Color FG_COLOR = {255, 255, 255};
    SDL_Color BG_COLOR = {0, 0, 255};

    void setup() {
        SDL_Init( SDL_INIT_VIDEO );
        TTF_Init();
        WINDOW = SDL_SetVideoMode(WIDTH, HEIGHT, 0,
            SDL_HWSURFACE | SDL_DOUBLEBUF);
        SDL_WM_SetCaption(TITLE, 0);
    }

    void shutdown() {
        if (SCENE) {
            delete SCENE;
            SCENE = NULL;
        }
        if (FONT) {
            TTF_CloseFont(FONT);
            FONT = NULL;
        }
        if (FONT_BIG) {
            TTF_CloseFont(FONT_BIG);
            FONT_BIG = NULL;
        }
        SDL_Quit();
    }

    void title() {
        if (SCENE) {
            delete SCENE;
            SCENE = NULL;
        }
        SCENE = new Scene("res/background/title.png");
        SCENE->draw();
    }

    void draw() {
        SCENE->draw();
    }

    Scene::Scene() {
        bg_src = NULL;
        bg_dest = NULL;
        bg = NULL;
    }

    Scene::Scene(const char* bg_path) {
        bg_src = new SDL_Rect();
        bg_src->x = 0;
        bg_src->y = 0;
        bg_src->w = WIDTH;
        bg_src->h = HEIGHT;
        bg_dest = new SDL_Rect();
        bg_dest->x = 0;
        bg_dest->y = 0;
        bg_dest->w = WIDTH;
        bg_dest->h = HEIGHT;
        bg = IMG_Load(bg_path);
    }

    Scene::~Scene() {
        if (bg_src) {
            delete bg_src;
            bg_src = NULL;
        }
        if (bg_dest) {
            delete bg_dest;
            bg_dest = NULL;
        }
        if (bg) {
            SDL_FreeSurface(bg);
            bg = NULL;
        }
    }

    void Scene::draw() {
        SDL_BlitSurface(bg, bg_src, WINDOW, bg_dest);
        SDL_Flip(WINDOW);
    }

}

