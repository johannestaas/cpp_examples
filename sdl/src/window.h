#ifndef __WINDOW__
#define __WINDOW__

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

namespace window {

    extern SDL_Surface *WINDOW;

    extern const int WIDTH;
    extern const int HEIGHT;
    extern const char *TITLE;
    extern TTF_Font* FONT;
    extern TTF_Font* FONT_BIG;
    extern SDL_Color FG_COLOR;
    extern SDL_Color BG_COLOR;

    void setup();
    void shutdown();

    void draw();
    void title();

    class Scene {
      private:
        SDL_Surface *bg;
        SDL_Rect *bg_src;
        SDL_Rect *bg_dest;
      public:
        Scene();
        Scene(const char* bg_path);
        ~Scene();
        void draw();
    };

    extern Scene *SCENE;

}

#endif
